package Crawler;

use 5.010;
use strict;
use warnings;

use AnyEvent::HTTP;
use Web::Query;
use URI;

use DDP;

=encoding UTF8

=head1 NAME

Crawler

=head1 SYNOPSIS

Web Crawler

=head1 run($start_page, $parallel_factor)

Сбор с сайта всех ссылок на уникальные страницы

Входные данные:

$start_page - Ссылка с которой надо начать обход сайта

$parallel_factor - Значение фактора паралельности

Выходные данные:

$total_size - суммарный размер собранных ссылок в байтах

@top10_list - top-10 страниц отсортированный по размеру.

=cut

sub run {
    my ($start_page, $parallel_factor) = @_;
    $start_page or die "You must setup url parameter";
    $parallel_factor or die "You must setup parallel factor > 0";

    my $total_size = 0;
    my @top10_list;

    my @urls = ($start_page);
    my (%hrefs_uniq, %hrefs) = ();
    my $quoted_start_page = quotemeta($start_page);

    local $AnyEvent::HTTP::MAX_PER_HOST = $parallel_factor;

    my $cv = AnyEvent->condvar;
    my $next;
    $cv->cb(sub {
        my @sorted_keys = sort { $hrefs{$b} <=> $hrefs{$a} } keys %hrefs;
        foreach (@sorted_keys)          { $total_size += $hrefs{$_}; }
        foreach (@sorted_keys[0..9])    { push @top10_list, $_; }
    });


    $cv->begin;
    $next = sub {
        my $url = shift @urls;
        return if (not $url or keys(%hrefs) >= 1000);
        $cv->begin;
        http_head $url, sub {
            my ($body, $header) = @_;
            if ($header->{"content-type"} =~ "text/html") {
                $cv->begin;
                http_get $url, sub {
                    my ($body, $header) = @_;
                    die("Fail processing $url: @$header{qw(Status Reason)}") if ($header->{Status} != 200);
                    $hrefs{$url} = $header->{"content-length"};
                    wq($body)
                        ->find('a')
                        ->each(sub {
                            $_->attr('href') =~ /(\/[^#].+)/;
                            my $new_abs = URI->new_abs($1, $start_page)->canonical;
                            if (!exists $hrefs_uniq{$new_abs} and $new_abs =~ $quoted_start_page) {
                                $hrefs_uniq{$new_abs} = 1;
                                push @urls, $new_abs;
                            }
                        });
                    $next->();
                    $cv->end;
                }
            }
            $next->();
            $cv->end;
        }
    }; $next->() for 1..$parallel_factor;
    $cv->end;

    $cv->recv;

    return $total_size, @top10_list;
}

1;
